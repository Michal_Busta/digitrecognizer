
# Digit Recognizer

to generate training data run: 

```
python gen_data.py
```

to increase accuracy, run data generator for several times ... 

to train and validate: 

```
python train.py  batch_size=128 data_dir=<path to you images> 

```

