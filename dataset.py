'''
Created on Nov 22, 2019

@author: michal.busta at gmail.com
'''

import albumentations as albu
from albumentations.pytorch import ToTensor

from torch.utils.data import DataLoader, Dataset, ConcatDataset

import numpy as np
import cv2

import os, glob

mean=(0.485, 0.456, 0.406)
std=(0.229, 0.224, 0.225)

from sklearn.model_selection import KFold



def get_training_augmentation(height=320, width=640, grayscale = True):
    
  mea = mean
  st = std
  if grayscale: 
    mea = np.asarray(mea).mean()
    st = np.asarray(st).mean()
  
  train_transform = [
    #albu.CoarseDropout(p=0.1),
    albu.ShiftScaleRotate(scale_limit=(-0.2, 0.2), rotate_limit=10, shift_limit=0.1, p=0.5, border_mode=0, interpolation=cv2.INTER_CUBIC),
    #albu.CoarseDropout(max_holes=4, max_height=4, max_width=4), 
    albu.GridDistortion(p=0.3),
    albu.PadIfNeeded(height, width),
    albu.CenterCrop(height, width),
    albu.Normalize(mean=mea, std=st, p=1),
    ToTensor(),
  ]    
  return albu.Compose(train_transform, additional_targets={"image2" : "image"})

def get_validation_augmentation(height=320, width=640, grayscale = True):
  """Add paddings to make image shape divisible by 32"""   
  mea = mean
  st = std
  
  if grayscale: 
    mea = np.asarray(mea).mean()
    st = np.asarray(st).mean()
      
  test_transform = [
      albu.PadIfNeeded(height, width),
      albu.CenterCrop(height, width),
      albu.Normalize(mean=mea, std=st, p=1),
      ToTensor(),
    ]
  
  return albu.Compose(test_transform, additional_targets={"image2" : "image"})

def get_test_augmentation(height=320, width=640):
  """Add paddings to make image shape divisible by 32"""   
  mea = mean
  st = std
  test_transform = [
      #albu.PadIfNeeded(height, width),
      albu.Resize(height, width, interpolation=cv2.INTER_CUBIC),
      albu.Normalize(mean=mea, std=st, p=1),
      ToTensor(),
  ]
  
  return albu.Compose(test_transform, additional_targets={"image2" : "image"})


class CCDataset(Dataset):
  def __init__(self, ids, transform = albu.Compose([albu.HorizontalFlip(), ToTensor()]), scale_factor=1.0, 
               is_train=True, phase='train'):
             
    self.img_ids = ids
    self.transform = transform
    self.scale_factor = scale_factor
    self.is_train = is_train
    self.phase = phase
      
  def __getitem__(self, idx):
    
    img_name = self.img_ids[idx]
    dir  = os.path.dirname(img_name)
    class_no = int(os.path.basename(dir))
    img = cv2.imread(img_name, cv2.IMREAD_GRAYSCALE)
    
    augmented = self.transform(image=img)
    img = augmented['image']
    img = img.unsqueeze(0)
    
    return img, class_no
  

  def __len__(self):
      
    return len(self.img_ids)
  

def provider(
        base_dir,
        phase,
        batch_size=4,
        num_workers=2,
        train_width = 32*4,
        train_height = 32,
        fold = 0,
        debug = False,
        scale_factor=1.0,
        return_filename = False, 
        ):
  '''Returns dataloader for the model training'''
  
  folds = 9
  sarpaths = glob.glob(os.path.join(base_dir, '*/*.jpg'))
  kf = KFold(n_splits=folds, shuffle=True, random_state=777)
      
  cf = 0
  for train_idx, valid_idx in kf.split(sarpaths):
      
    if debug:
      train_idx = train_idx[0:500]
      valid_idx = valid_idx[0:500]
  
    if cf != fold:
      cf += 1
      continue
    sarpaths = np.asarray(sarpaths)
    if  phase == 'train':    
      train_dataset = CCDataset(sarpaths[train_idx], transform = get_training_augmentation(width=train_width, height=train_height), scale_factor=scale_factor)
    else:
      train_dataset = CCDataset(sarpaths[valid_idx], transform = get_validation_augmentation(width=train_width, height=train_height), scale_factor=scale_factor, is_train = not return_filename, phase=phase)
    break
  
  if phase == 'train' and os.path.exists(f'generated'):
    
    print('Using generated data')
    sarpaths2 = glob.glob(os.path.join(base_dir, 'generated/*/*.jpg'))
    train_datasetg = CCDataset(sarpaths2, transform = get_training_augmentation(width=train_width, height=train_height), scale_factor=scale_factor)
    
    final_dataset = ConcatDataset([train_dataset, train_datasetg]) 
    
    dataloader = DataLoader(
      final_dataset,
      sampler= None,
      batch_size=batch_size,
      num_workers=num_workers,
      pin_memory=True,
      shuffle=True if phase=='train' else False,
      drop_last=True     
    )
    
  else:
  
    dataloader = DataLoader(
      train_dataset,
      sampler= None,
      batch_size=batch_size,
      num_workers=num_workers,
      pin_memory=True,
      shuffle=True if phase=='train' else False,
      drop_last=True     
    )
  return dataloader


      

