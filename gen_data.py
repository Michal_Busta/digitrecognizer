'''
Created on Dec 5, 2017

@author: michal.busta at gmail.com
'''

from PIL import Image

from bidi.algorithm import get_display

from PIL import ImageFont
from PIL import ImageDraw 

import random

import numpy as np

import cv2

import os
import math

import freetype as ft

def get_fonts(font_dir):
  
  font_list = []
  
  for root, dirs, files in os.walk(font_dir):
    for ft_dir in dirs:
      print(ft_dir)
      cdir = os.path.join(root, ft_dir)
      for _, _, files2 in os.walk(cdir):
        for f in files2:
          if f.endswith('.ttf'):
            print(f)
            font_list.append(os.path.join(cdir, f))
  
  return font_list

if __name__ == '__main__':
  
  out_dir = 'generated'
  if not os.path.exists(out_dir):
    os.makedirs(out_dir)
  
  font_list = get_fonts('Latin')
    
  for i in range(0, 30000):
  
    class_no = random.randint(0, 199)
    reshaped_text = str(class_no)
    
    out_dir2 = f'{out_dir}/{class_no}'
    if not os.path.exists(out_dir2):
      os.makedirs(out_dir2)
  
    # At this stage the text in bidi_text can be easily rendered in any library
    # that doesn't support Arabic and/or right-to-left, so use it as you'd use
    # any other string. For example if you're using PIL.ImageDraw.text to draw
    # text over an image you'd just use it like this...
    font_name = font_list[random.randrange(0, len(font_list))]
    
    face = ft.Face(font_name)
    face.set_pixel_sizes(25, 32)
    font2 = ImageFont.truetype(font_name, 12)    
    image = Image.new('RGBA', (100, 100), (255,255,255,0))
    image_draw = ImageDraw.Draw(image)
    
    text_size = image_draw.textsize(reshaped_text, font2)
    text_sizeh = text_size[1] + random.randint(1, 5)
    text_size = int(text_size[0] * 2) + random.randint(10, 20)
    text_size = (text_size,  text_sizeh)
    
    image = Image.new('RGBA', text_size, (random.randint(128, 255),random.randint(128, 255),random.randint(128, 255),0))
    image_draw = ImageDraw.Draw(image)
    
    rsx = random.randint(-2, 10)
    image_draw.text((10 + rsx,2 + random.randint(-1, 2) ), reshaped_text, (0,0,0), font=font2)
    
    im = np.array(image)
    #cv2.imshow('img', im)
    #cv2.waitKey(0)
    image_name = '{0}/{1}.jpg'.format(out_dir2, i)
    width = int(text_size[0] / (text_size[1] / 32))
    im = cv2.resize(im, (width, 32), interpolation=cv2.INTER_CUBIC)
    im = ~im
    cv2.imwrite(image_name, im)
    
    #cv2.imshow('img', im)
    #cv2.waitKey(0)
    
    
  
  
  