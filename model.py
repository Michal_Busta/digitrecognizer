'''
Created on Jun 16, 2020

@author: Michal.Busta at gmail.com
'''

'''
Created on Feb 26, 2020

@author: Michal.Busta at gmail.com
'''

import torch
import torch.nn as nn
from fastai.vision import *

arch = models.densenet121

def constant_init(module, val, bias=0):
  if hasattr(module, 'weight') and module.weight is not None:
    nn.init.constant_(module.weight, val)
  if hasattr(module, 'bias') and module.bias is not None:
    nn.init.constant_(module.bias, bias)

def last_zero_init(m):
  if isinstance(m, nn.Sequential):
    constant_init(m[-1], val=0)
    m[-1].inited = True
  else:
    constant_init(m, val=0)
    m.inited = True
    
class ContextBlock2d(nn.Module):

  def __init__(self, inplanes, planes, pool='att', fusions=['channel_add'], ratio=8):
    super(ContextBlock2d, self).__init__()
    assert pool in ['avg', 'att']
    assert all([f in ['channel_add', 'channel_mul'] for f in fusions])
    assert len(fusions) > 0, 'at least one fusion should be used'
    self.inplanes = inplanes
    self.planes = planes
    self.pool = pool
    self.fusions = fusions
    if 'att' in pool:
      self.conv_mask = nn.Conv2d(inplanes, 1, kernel_size=1)#context Modeling
      self.softmax = nn.Softmax(dim=2)
    else:
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
    if 'channel_add' in fusions:
      self.channel_add_conv = nn.Sequential(
          nn.Conv2d(self.inplanes, self.planes // ratio, kernel_size=1),
          nn.LayerNorm([self.planes // ratio, 1, 1]),
          nn.ReLU(inplace=True),
          nn.Conv2d(self.planes // ratio, self.inplanes, kernel_size=1)
      )
    else:
      self.channel_add_conv = None
    if 'channel_mul' in fusions:
      self.channel_mul_conv = nn.Sequential(
          nn.Conv2d(self.inplanes, self.planes // ratio, kernel_size=1),
          nn.LayerNorm([self.planes // ratio, 1, 1]),
          nn.ReLU(inplace=True),
          nn.Conv2d(self.planes // ratio, self.inplanes, kernel_size=1)
      )
    else:
      self.channel_mul_conv = None
    self.reset_parameters()

  def reset_parameters(self):
    #if self.pool == 'att':
    #  nn.init.kaiming_init(self.conv_mask, mode='fan_in')
    #  self.conv_mask.inited = True

    if self.channel_add_conv is not None:
      last_zero_init(self.channel_add_conv)
    if self.channel_mul_conv is not None:
      last_zero_init(self.channel_mul_conv)

  def spatial_pool(self, x):
    batch, channel, height, width = x.size()
    if self.pool == 'att':
      input_x = x
      # [N, C, H * W]
      input_x = input_x.view(batch, channel, height * width)
      # [N, 1, C, H * W]
      input_x = input_x.unsqueeze(1)
      # [N, 1, H, W]
      context_mask = self.conv_mask(x)
      # [N, 1, H * W]
      context_mask = context_mask.view(batch, 1, height * width)
      # [N, 1, H * W]
      context_mask = self.softmax(context_mask)#softmax操作
      # [N, 1, H * W, 1]
      context_mask = context_mask.unsqueeze(3)
      # [N, 1, C, 1]
      context = torch.matmul(input_x, context_mask)
      # [N, C, 1, 1]
      context = context.view(batch, channel, 1, 1)
    else:
      # [N, C, 1, 1]
      context = self.avg_pool(x)

    return context

  def forward(self, x):
      # [N, C, 1, 1]
    context = self.spatial_pool(x)

    if self.channel_mul_conv is not None:
      # [N, C, 1, 1]
      channel_mul_term = torch.sigmoid(self.channel_mul_conv(context))
      out = x * channel_mul_term
    else:
      out = x
    if self.channel_add_conv is not None:
      # [N, C, 1, 1]
      channel_add_term = self.channel_add_conv(context)
      out = out + channel_add_term

    return out

class Mish(nn.Module):
  def __init__(self):
    super().__init__()

  def forward(self, x):
    #inlining this saves 1 second per epoch (V100 GPU) vs having a temp x and then returning x(!)
    return x *( torch.tanh(F.softplus(x)))

class Head(nn.Module):
  def __init__(self, nc, n, ps=0.5):
    super().__init__()
    
    self.att = ContextBlock2d(nc, nc // 2)
    layers = [AdaptiveConcatPool2d(), Mish(), Flatten()] + \
        bn_drop_lin(nc*2, 512, True, ps, Mish()) + \
        bn_drop_lin(512, n, True, ps)
    self.fc = nn.Sequential(*layers)
    self._init_weight()
      
  def _init_weight(self):
    for m in self.modules():
      if isinstance(m, nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight)
      elif isinstance(m, nn.BatchNorm2d):
        m.weight.data.fill_(1.0)
        m.bias.data.zero_()
    
  def forward(self, x):
    x = self.att(x)
    return self.fc(x)

#change the first conv to accept 1 chanel input
class Dnet_1ch(nn.Module):

  def __init__(self, arch_fn=arch, n=10, pre=True, ps=0.5):
    super().__init__()
    m = arch(True) if pre else arch()
    
    conv = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
    w = (m.features.conv0.weight.sum(1)).unsqueeze(1)
    conv.weight = nn.Parameter(w)
    
    self.layer0 = nn.Sequential(conv, m.features.norm0, Mish())
    self.layer1 = nn.Sequential(
        nn.MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False),
        m.features.denseblock1)
    self.layer2 = nn.Sequential(m.features.transition1,m.features.denseblock2)
    self.layer3 = nn.Sequential(m.features.transition2,m.features.denseblock3)
    self.layer4 = nn.Sequential(m.features.transition3,m.features.denseblock4,
                                m.features.norm5)
    
    nc = self.layer4[-1].weight.shape[0]
    #self.context0 = ContextBlock2d(1024, 1024 // 2)
    self.head = Head(nc,n)
      
  def forward(self, x):    
    x = self.layer0(x)
    x = self.layer1(x)
    x = self.layer2(x)
    x = self.layer3(x)
    x = self.layer4(x)
    x = self.head(x)
        
    return x 
  
  