'''
Created on Jun 16, 2020

@author: michal.busta at gmail.com
'''

from dataset import provider

import fastai
from fastai.vision import *
from fastai.callback import Callback
from fastai.callbacks import SaveModelCallback
import os
import warnings
warnings.filterwarnings("ignore")

print(fastai.__version__)
import hydra

from optim import Over9000
from model import Dnet_1ch

import random
import numpy as np
import torch

from fastai.basic_data import DataBunch

def seed_everything(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True
    

  

@hydra.main(config_path='config/config.yaml')
def main(cfg):
  print(cfg.pretty())
  
  model = Dnet_1ch(pre=True, n=200).cuda()
  #model = model.cuda()
  if os.path.exists(cfg.model):
    state = torch.load(cfg.model, map_location=lambda storage, loc: storage)
    model.load_state_dict(state["model"])
    print('state ok')
  
  SEED = 2020
  seed_everything(SEED)
  
  try:
    os.mkdir('weights')
  except:
    pass
  
  batch_size = cfg.batch_size
  print(f'working batch size: {batch_size}')
  workers = 1
  
  train_dataset = provider(
        cfg.data_dir,
        'train',
        batch_size=batch_size,
        num_workers=workers,
        train_width = cfg.width,
        train_height = cfg.height,
        fold = cfg.fold,
        debug = cfg.debug,
        scale_factor = cfg.scale_factor,
      )
  
  valid_dataset = provider(
        cfg.data_dir,
        'val',
        batch_size=batch_size,
        num_workers=workers,
        train_width = cfg.width,
        train_height = cfg.height,
        fold = cfg.fold,
        debug = cfg.debug,
        scale_factor = cfg.scale_factor,
      )
  
  data = DataBunch(train_dataset, valid_dataset) 

  learn = Learner(data, model, loss_func=torch.nn.CrossEntropyLoss(), opt_func=Over9000, model_dir='weights', metrics=accuracy).mixup()
  
  if cfg.validate == 1:
    model.eval()
    result = learn.validate()
    print(f'validation result {result}')
  else:
    learn.clip_grad = 1.0
    learn.split([model.head])
    learn.unfreeze()
    
    
    learn.fit_fc(30, lr=np.array([0.2e-2,1e-1]), wd=[cfg.weight_decay * 0.2, cfg.weight_decay], 
      callbacks = [SaveModelCallback(learn,monitor='accuracy',
      mode='max',name=f'model')], start_pct=0.25)
    
    #learn.fit_one_cycle(20, max_lr=slice(0.2e-2,1e-1), wd=[1e-3,0.1e-1], pct_start=0.0, 
    #  div_factor=100, callbacks = [SaveModelCallback(learn,monitor='accuracy',
    #  mode='max',name=f'model')])
    
  
if __name__ == '__main__':
  main()
  
  